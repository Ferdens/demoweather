//
//  DataStorage.swift
//  AlamofireTesting
//
//  Created by Anton on 12.12.16.
//  Copyright © 2016 Macbook. All rights reserved.
//

import Foundation



struct DataStorage {
    
    var cityesDict = ["London"    :2648110, "Washington":4450315,
                      "Amsterdam" :2759793,"Cecil"      :4350685,
                      "Kiev"      :703448 ,"Moscow"     :524901]
    
    var nameOfCityes = ["London"    ,"Washington",
                        "Amsterdam" ,"Cecil"     ,
                        "Kiev"      ,"Moscow"    ]
    
    let pres        = "Pressure(gPA): "
    let temp        = "Temperature(С): "
    let windSp      = "Wind speed(km/h):"
    let visibility  = "Visibility(m): "
    
}
