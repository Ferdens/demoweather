//
//  Network.swift
//  AlamofireTesting
//
//  Created by Anton on 05.12.16.
//  Copyright © 2016 Macbook. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
class Network {
    
    var (pres,temper,windSp,visib) = ("","","","")
    func wheatherInfoByCityID(cityID: Int) -> (pressure: String,temperature: String,windSpeed: String,visibility:String) {
        
        Alamofire.request("http://api.openweathermap.org/data/2.5/weather?id=\(cityID)&APPID=472d16269c2f48fb15d2912ba6dc8827").responseJSON(completionHandler: {
            (response) in
            if let JSON = response.result.value{
                if let resp = JSON as? NSDictionary {
                    let main        = resp["main"] as! NSDictionary
                    let pressure    = main["pressure"]
                    let temp        = main["temp"]
                    let visibility  = resp["visibility"]
                    let wind        = resp["wind"] as! NSDictionary
                    let windSpeed   = wind["speed"]
                    self.pres       = String(describing: pressure!)
                    self.temper     = String(describing: temp!)
                    self.windSp     = String(describing: windSpeed!)
                    self.visib      = String(describing: visibility!)
                }
            }
        })
        return (self.pres,self.temper,self.windSp,self.visib)
    }
    
}
