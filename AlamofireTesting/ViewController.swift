//
//  ViewController.swift
//  AlamofireTesting
//
//  Created by Anton on 05.12.16.
//  Copyright © 2016 Macbook. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource{
    
    var net  = Network()
    var response : (String,String,String,String) = ("","","","")
    @IBOutlet weak var pressureResult   : UILabel!
    @IBOutlet weak var tempResult       : UILabel!
    @IBOutlet weak var windSpResult     : UILabel!
    @IBOutlet weak var visibilityResult : UILabel!
    @IBOutlet weak var cityesPicker     : UIPickerView!
    
    var cityesDict   = DataStorage().cityesDict
    var nameOfCityes = DataStorage().nameOfCityes
    let pres         = DataStorage().pres
    let temp         = DataStorage().temp
    let windSp       = DataStorage().visibility
    let visibility   = DataStorage().windSp
    
    //MARK: MyMethods
    
    @IBAction func wheatherData(_ sender: UIButton) {
      
        let cityInPicker =  self.cityesPicker.selectedRow(inComponent: 0)
        self.response = net.wheatherInfoByCityID(cityID: cityesDict[nameOfCityes[cityInPicker]]!)
        var temperatureResult = String(Int((Double(self.response.1) ?? 0.0) - 273.15))
        if Int(temperatureResult) == -273 {
            temperatureResult = ""
        }
        self.pressureResult.text    = pres + self.response.0
        self.tempResult.text        = temp + temperatureResult
        self.windSpResult.text      = windSp + self.response.3
        self.visibilityResult.text  = visibility + self.response.2
    }
    
    //MARK: PickerDataSourceMethods
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return nameOfCityes.count
    }
    
    //MARK: PickerDelegateMethods
    
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return nameOfCityes[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
         self.response = net.wheatherInfoByCityID(cityID: cityesDict[nameOfCityes[row]]!)
    }
    
}

