//
//  SecondViewController.swift
//  AlamofireTesting
//
//  Created by Anton on 05.12.16.
//  Copyright © 2016 Macbook. All rights reserved.
//

import UIKit
protocol SecondViewControllerDelegate {
    func addData(cityName: String,cityID: Int)
}

class SecondViewController: UIViewController {

    var delegate: SecondViewControllerDelegate?
    
    @IBOutlet weak var cityName: UITextField!
    @IBOutlet weak var cityID: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func sendDataToFirstVC(_ sender: Any) {
        
        let cityName = self.cityName.text
        let cityID   = self.cityID.text
        delegate?.addData(cityName: cityName!, cityID: Int(cityID!)!)
        navigationController?.popViewController(animated: true)
      
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
